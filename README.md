# Quote of the Day - Version 3.0

The Quote of the Day (QotD) application is demonstration micro-service application, specifically for use with IBM Watson AIOps.  It is optionally instrumented for Instana, and provides a wealth of logs and metrics (Prometheus compatible).

## What it does

The application itself is very simple.  Navigate to the home page and you will be greeted with a quote from a famous (or nearly famous) person that has been selected for the current day of the year.  The quotes come from a database of over 500 quotes, and have been categorized by genre. Each quote is rated (with a banner of stars). The user can then optionaly request a new random quote, or request the quote in PDF form.  The user can click on the authors link to get a short biography and picture (graciously obtained from wikipedia).

In this latest release (3.x) an additional service has been added that allows the user to request an engraving.  If configured, this engraving service makes a call to an IBM App Connect Enterprise (ACE), which in turn adds a message on IBM MQ.  The entire flow is traced with Instana, showing off the full end to end traceability that Instana offers.

![home page](images/home_page.png)

Some of the authors bio's and pictures don't exist and these will result in errors added to the log.  This is expected, and part of the overall function of the app.

Additionally the latest 3.x versions allow an additional, per service, configuration which makes any of the service end points randomly throw a non 2xx response.  For example the default configuration of the rating service includes a ConfigMap that specifies that 1% of the calls to the rating service will result in a 404 Not Found error.  This error, and the additional log entries that it generates will be part of the normal operation, and hence should be part of any training that Watson AIOps does.

In addition to the application itself there is a load generator and anomaly generator, that is expected to be deployed with the application.  The load and anomaly generator can be deployed to the same namespace as the app (simpler), or can be deployed to another namespace (preferred).  If deployed to another namespace, ensure the network policies are such that the `qotd` project pods can accept connections from other namespaces, or update the URL references in the environment variables for load generator and anomaly generator to use the public routes of the main QotD services. (This is discussed in more detail in the Installation documentation).

## Installation

This application does not have a helm chart (yet), nor has an operator been written for Kubernetes deployments.  Therefore you will need to manually deploy the components directly from yaml files.

[Deploy to OpenShift](ocp4_deployment.md)

[Deploy to Generic Kubernetes](k8s_deployment.md)

## Service Architecture

The QotD application is spread out over 7 services and one database, as shown in the figure below.  The engraving service is an optional component and typically not part of the default installation, since it is expected to connect to an already configured ACE restpoint.

![Service dependencies overview](images/service-dependencies.png)

The arrows in the diagram indicate a dependency, or more specifically they represent a call from the one component to another.  All calls in the outward function of this application are HTTP REST (GET) calls.

Each service generates its own logs and provides metrics.  Prometheus compatible metrics can be scraped off of each service at the `/metrics` endpoint.  Each service is also instrumented for Instana, which also collects metrics.

![Instana view](images/instana_dependencies.png)

## Components

Each service in this demo application is managed by its own GitLab project.  Presently each service is implemented with Node.js, however this is not a requirement for any future additions.  All of the services that can generate anomaloous behavior share a set of common files (`utils.js`, `logger.js`, `metrics.js` and `serviceController.js`), even though most of the services share several common files.  (This sharing of files is still a manual process at this point).

In general the 7 components that make up the business part of the application expose two endpoints; one on port 300x and another on port 808x where x is different for each service.  For example the web service exposes ports 3000 and 8080.  The 3000 level port is for application (business) logic.  It represents the actual functionality of the service.  The load generator and other services use this port to exercise the quote of the day functionality.

![Instana view](images/generic_service.png)

The second exposed port in the range of 8080 to 8087 (dependeing on component) is dedicated to anomaly genertation.  The anomaly generator application uses this port to send the service commands to change its behavior.  Anomalous behaviors can be any of the following.

- CPU usage
- Memory Usage
- Latency
- HTTP response codes (i.e. 400, 404, 500, ...)
- Log message frequencies or new log messages

When it comes to CPU, memory and latency, the changes are real.  For example to increase the CPU usage, the code continuously computes random numbers.  Memory consumption is produced by creating large arrays and stuffing them with integer values (which not surprisingly causes a temporary increase in CPU usage).  Every business service already has a built in latency of 100ms, but this can extended with the anomaly generator to about 1.5 seconds.

### Web 

The [web](https://gitlab.com/quote-of-the-day/qotd-web) component is the web front end of the application and the starting point for all scenarios in the application.  It is implemented as a Node.js Express application, and like all the service components has additional APIs that are called by the anomaly generator, which can introduce new log messages, modify the CPU and memory metrics as well as modify the built in time delay of every API (and web) call.

### Quote 

The [quote](https://gitlab.com/quote-of-the-day/quote-service) service provides the quotes to the web tier.  It queries a MariaDB database with over 500 quotes in it.  The quote database includes only the quote, author name and genre.  It does not include author bio information.  This is provided by the author service.

### Rating

The [rating]() service provides a rating.  The rating is actually randomly generated, but conceptually the information might come from another database.  The rating is 0 to 5 stars, with half stars allowed, which effectively permits an integer value of 0 to 10 (inclusive).

### Author

The [author]() service provides biographical information of an author.  A link around the author's name driects the browser to the author's page.  The web front end calls the author service to get the author's biographical information (as found on Wikipedia), and also calls it for the author's image.  The author service then calls the image service to get an actual image (if available).  

### Image

The [image]() service provide JPG images of authors. Not all authors have an image available, in which case a placeholder is returned.

### PDF

The [pdf]() service generates a new PDF resource with the quote and author name in it.  The PDF is generated on the fly with the help of the [`pdf-lib` module](https://www.npmjs.com/package/pdf-lib). 

## Load Scenarios

The [load generator](https://gitlab.com/quote-of-the-day/qotd-load-generator) for this application has 6 configurable use cases (scenarios).  Each use case is metadata defined, so that it is possible to easily create additional scenarios, both for load and anomaly generation (however the features to do this at runtime are not yet built, and any permanent changes to the scenarios require a rebuild of the app). (note: the value in the square brackets indicates the total number of log entries produced by the use case)

- [Daily Quote](uc_daily.md) [11]
- [Random Quote](uc_random.md) [23]
- [Author Daily](uc_author_daily.md) [21]
- [Author Random](uc_author_random.md) [35]
- [PDF Daily](uc_pdf_daily.md) [21]
- [PDF Random](uc_pdf_random.md) [34]

In all of the scenarios there is an optional delay after executing each step.  This delay is either exactly specified or nominally specified, in which case the delay time is generated from a normal distribution with a given mean and standard deviation (and optional upper and lower limits). 

The load varys according to the time of day. Each round of use case executions (one run of each of the above use cases) results in the generation of 145 log entries.  This means the variation of the number of log messages is anywhere from 145 to about 725 a minute.  Other messages may be sent to the log aggregator (i.e. Instana), so the total number of log messages at any given time will be slightly higher.

![Use case execution schedule](images/schedule.png)


## Anomaly Scenarios

More involved scenarios can be defined and run against the application, with the ability to manipulate the metrics, response time and introduce new log entries.  The following scenarios have been pre-defined.  Future releases of this application (assuming anyone uses it), will make it much easier to define your own scenarios and add it to the Use Case generator application.

This application makes heavy use of a normal distribution [number generator](https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform).  It is used to vary the delays between execution of use case steps, as well as in the api response time of all the app's components as well as the load generator.

- [Ratings service failure](uc_rating_failure.md)
- [Author and Image Service Slowage](./uc_author_image_slow.md)
- [Rating, author, image service cascade failure](uc_rating_author_image_failure.md)

![anomaly home](images/anomaly_home.png)

Note: if you stop the anomaly before it completes itself, then it will remain in the state it was stopped.  You will probably want to go back to the home page and press the **Reset All Services to Factory Settings** button to ensure the application's components are in a known state.

As the anomaly plays out, the UI will indicate completed and current tasks.

![anomaly run](images/anomaly_run.png)


## Managing Anomalies (Use Cases)

You can add your own anomalies to the default list included with this app.  These anomalies (use cases) must be constructed as JSON objects and uploaded to this application.  See the documentation of the [Anomaly Generator](anomaly_generator.md) for details.
