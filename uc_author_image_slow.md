# Anomaly: Author and Image Services Slowage

This use case executes the following steps:

1. Several new types of log entries are introduced in the Author and Image services. The templates for these are shown below.

    * Added in Author service about every second:
        ```
        WARNING incoming {{VERB}} request from {{IP}} rejected 
        ```

    * Added in Author service about every three seconds:
        ```
        DANGER Will Robinson. Unexpected request for {{URL}} from source: {{IP}}
        ```

    * Added in Image service about every three seconds:
        ```
        ERROR possible hacking to access resource {{URL}} from source: {{IP}} 
        ```

    * Added in Image service about every second:

        ```
        DANGER external {{VERB}} request from known malicious {{IP}} was rejected 
        ```

1. The Author and Image services latency is set to delay 2 seconds nominally.

1. After about 5 seconds, no more anomalous log entries are introduced in the Author and Image services, while the introduced latency in both services is kept.

## Source

```json
{
  "id": "image_author_slow",
  "name": "Author and Image Service Slowage",
  "description": "New log entries are added to author and image services. They both experience sharp increases in response time of their primary services, near 2 seconds.",
  "steps": [
    {
      "name": "Start log warning about requests for resource from IP address (author service). Repeats every 3 seconds.",
      "type": "add_logger",
      "service": "author",
      "log": {
        "id": "resreq1",
        "name": "dangerwill",
        "template": "DANGER Will Robinson. Unexpected request for {{URL}} from source: {{IP}} ",
        "fields": {
          "URL": {
            "type": "url"
          },
          "IP": {
            "type": "ip"
          }
        },
        "repeat": {
          "mean": 3000,
          "stdev": 100,
          "min": 2000,
          "max": 4000
        }
      }
    },
    {
      "name": "Start log warning about requests for resource from IP address (image service). Repeats every 3 seconds.",
      "type": "add_logger",
      "service": "image",
      "log": {
        "id": "resreq1",
        "name": "possible hack",
        "template": "ERROR possible hacking to access resource {{URL}} from source: {{IP}} ",
        "fields": {
          "URL": {
            "type": "url"
          },
          "IP": {
            "type": "ip"
          }
        },
        "repeat": {
          "mean": 3000,
          "stdev": 100,
          "min": 2000,
          "max": 4000
        }
      }
    },
    {
      "name": "Start log warning about incoming HTTP requests from external IP address (author service). Repeats about every second.",
      "type": "add_logger",
      "service": "author",
      "log": {
        "id": "in_req",
        "name": "warn incoming",
        "template": "WARNING incoming {{VERB}} request from {{IP}} rejected ",
        "fields": {
          "IP": {
            "type": "ip"
          },
          "VERB": {
            "type": "pickWeighted",
            "options": [
              {
                "name": "GET",
                "value": "GET",
                "weight": 8
              },
              {
                "name": "POST",
                "value": "POST",
                "weight": 3
              },
              {
                "name": "PUT",
                "value": "PUT",
                "weight": 1
              },
              {
                "name": "DELETE",
                "value": "DELETE",
                "weight": 1
              },
              {
                "name": "HEAD",
                "value": "HEAD",
                "weight": 7
              }
            ]
          }
        },
        "repeat": {
          "mean": 1000,
          "stdev": 500,
          "min": 10,
          "max": 3000
        }
      }
    },
    {
      "name": "Start log warning about incoming HTTP requests from external IP address (author service). Repeats about every second.",
      "type": "add_logger",
      "service": "image",
      "log": {
        "id": "in_req",
        "name": "warn incoming",
        "template": "DANGER external {{VERB}} request from known malicious {{IP}} was rejected ",
        "fields": {
          "IP": {
            "type": "ip"
          },
          "VERB": {
            "type": "pickWeighted",
            "options": [
              {
                "name": "GET",
                "value": "GET",
                "weight": 8
              },
              {
                "name": "POST",
                "value": "POST",
                "weight": 3
              },
              {
                "name": "PUT",
                "value": "PUT",
                "weight": 1
              },
              {
                "name": "DELETE",
                "value": "DELETE",
                "weight": 1
              },
              {
                "name": "HEAD",
                "value": "HEAD",
                "weight": 7
              }
            ]
          }
        },
        "repeat": {
          "mean": 1000,
          "stdev": 500,
          "min": 10,
          "max": 3000
        }
      }
    },
    {
      "name": "Increase rating service delay (2s)",
      "type": "metric",
      "service": "author",
      "metric": "api",
      "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
      }
    },
    {
      "name": "Increase author service delay (2s)",
      "type": "metric",
      "service": "image",
      "metric": "api",
      "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
      }
    },
    {
      "name": "Pause 5 seconds",
      "type": "delay",
      "duration": "5000"
    },
    {
      "name": "Remove first logger",
      "type": "remove_logger",
      "service": "author",
      "logid": "resreq1"
    },
    {
      "name": "Remove second logger",
      "type": "remove_logger",
      "service": "author",
      "logid": "in_req"
    },
    {
      "name": "Remove first logger",
      "type": "remove_logger",
      "service": "image",
      "logid": "resreq1"
    },
    {
      "name": "Remove second logger",
      "type": "remove_logger",
      "service": "image",
      "logid": "in_req"
    }
  ]
}
```