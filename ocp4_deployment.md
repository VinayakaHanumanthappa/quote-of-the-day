# Openshift 4.x Deployment

These instructions are by no means the only way QotD can be deployed.  Feel free to distribute components across platforms (i.e. OCP, VMs, etc.).  This will mean you will have to make changes to the environment variables (and ConfigMaps).  Details of such deployments are, for the time, out of scope of this initial set of documentation.

## Quick Start

1. After logging into OCP with cluster admin role, create a project (namespace) for the application called `qotd` and another called `qotd-load` for the load and anomaly generators. Feel free to use another namespaces, however you will have to edit all the provided yaml files.

    ```shell
    oc new-project qotd
    oc new-project qotd-load
    ```

1. **Note:** Unfortunately the current version of the database component (based on [MariaDB base image](https://hub.docker.com/_/mariadb/)) runs as `root` user in the container.  This means you will have to give this container privledges to run in the `qotd` namespace.  

    ```shell
    oc project qotd
    oc adm policy add-scc-to-user anyuid -z default
    ```

1. The Kubernetes yaml files are in the `resources/k8s` directory of this repository. Before you can apply them, you must edit the database password  environment variable in the `qotd_db.yaml`, `qotd_author.yaml` and `qotd_quote.yaml` files.  At this time, the **value of the password must be `root`**. This is because that was the initial password used when the database was configured in the Dockerfile.  If anyone knows how to make this better please let me know!  Also if you are wondering why I just don't edit it for you, I can't. This is because security scanning processes will find it and make me change it - it already happened once :-). 

1. Edit `qotd_usecase.yaml`, replacing `<YOUR OCP HOST>` with the base hostname of your cluster (e.g. `cluster1.example.com`, `apps.demo-apps.cp.fyre.ibm.com`).

1. If you are not using Instana, you should edit the `qotd_author.yaml`, `qotd_image_yaml`, `qotd_pdf.yaml`, `qotd_quote.yaml`, `qotd_rating.yaml`, and `qotd_web.yaml` files, updating the value of `ENABLE_INSTANA` to `false`.

1. Run the `oc apply -f` command on all the files in the `k8s/ocp_okd` directory of this repository.  If you have cloned this repository, you can see multiple version of the application, so you can just run the following command from the top of the project based on the version you select to run.

    ```shell
    oc apply -f k8s/ocp_okd/<application_version>/
    ```

    You should see a confirmation of the creation of the new resources.

    ```shell
    deployment.apps/qotd-author created
    configmap/qotd-author-factory-settings created
    service/qotd-author created
    route.route.openshift.io/qotd-author created
    route.route.openshift.io/qotd-author-svc created
    deployment.apps/qotd-db created
    service/qotd-db created
    deployment.apps/qotd-image created
    configmap/qotd-image-factory-settings created
    service/qotd-image created
    service/qotd-image-svc created
    route.route.openshift.io/qotd-image created
    route.route.openshift.io/qotd-image-svc created
    deployment.apps/qotd-pdf created
    configmap/qotd-pdf-factory-settings created
    service/qotd-pdf created
    service/qotd-pdf-svc created
    route.route.openshift.io/qotd-pdf created
    route.route.openshift.io/qotd-pdf-svc created
    deployment.apps/qotd-quote created
    configmap/qotd-quote-factory-settings created
    service/qotd-quote created
    service/qotd-quote-svc created
    route.route.openshift.io/qotd-quote created
    route.route.openshift.io/qotd-quote-svc created
    deployment.apps/qotd-ratings created
    configmap/qotd-ratings-factory-settings created
    service/qotd-rating created
    service/qotd-rating-svc created
    route.route.openshift.io/qotd-rating created
    route.route.openshift.io/qotd-rating-svc created
    deployment.apps/qotd-web created
    configmap/qotd-web-factory-settings created
    service/qotd-web created
    service/qotd-web-svc created
    route.route.openshift.io/qotd-web created
    route.route.openshift.io/qotd-web-svc created
    deployment.apps/qotd-load created
    service/qotd-load created
    route.route.openshift.io/qotd-load created
    deployment.apps/qotd-usecase created
    service/qotd-usecase created
    route.route.openshift.io/qotd-usecase created
    ```

1. Give the system a chance to start up.  You should see three routes defined; the main web front end of the application which can be seen under `qotd` namespace, and two auxiliary applications, one for the load generator, and the other for a use case (anomaly) generator, which can be seen under `qotd-load` namespace.

    ![routes](images/routes_1.png)
    ![routes](images/routes_2.png)

1. Verify the app is working.  Navigate to the app's home page; route `qotd-web`.

    ![home page](images/home_page.png)

1. By default the load generator is not started.  Navigate to the `qotd-load` route.  Then press the Start button.  You will see the number of rounds of load use cases being updated every two seconds.

    ![load generator](images/load_home.png)

1. You can initiate an anomaly with the Anomaly generator app (separate from load generation).  Open it up with the `qotd-usecase` route. Select the anomaly you want to execute.

    ![Anomaly home page](images/anomaly_home.png)

1. Press the **Start** button on the anomaly use case page.  Each time a step is completed, the line will be marked with a green circle check.  A full green circle indicates the step is actively executing.  This page is auto refreshed every 2 seconds.

    ![Anomaly run](images/anomaly_run.png)

## Prometheus Resources

As a convenience to those using or wanting to use Prometheus on OpenShift to collect QotD metrics and generate alerts, we have included the OpenShift assets for doing so in the `resources/prometheus` (thanks Ricardo) folder. For deploying such assets to your OpenShift cluster, we assume you will use an OpenShift cluster administrator userID. Otherwise, you'll need to take additional steps for monitoring and collecting QotD metrics (doing so using a **non** administrative userID is beyond the scope of this document). Also, we assume you are using OpenShift version that supports monitoring of user-defined projects (for more details see [Enabling monitoring for user-defined projects](https://docs.openshift.com/container-platform/4.6/monitoring/enabling-monitoring-for-user-defined-projects.html) -- the commands below were tested on OpenShift v4.6).

1. First navigate to the `resources/prometheus` folder on your local workstation.

1. Using the OpenShift CLI, create the `cluster-monitoring-config` `ConfigMap` object in the `openshift-monitoring` project:

    ```
    oc apply -f cluster-monitoring-config.yaml
    ```

1. Wait for all pods in the `openshift-user-workload-monitoring` project to come up:

    ```
    oc get pods -n openshift-user-workload-monitoring
    ```

    ```
    NAME                                   READY   STATUS    RESTARTS   AGE
    prometheus-operator-8665f65566-g9mwx   2/2     Running   0          7d4h
    prometheus-user-workload-0             4/4     Running   1          7d4h
    prometheus-user-workload-1             4/4     Running   1          7d4h
    thanos-ruler-user-workload-0           3/3     Running   0          7d4h
    thanos-ruler-user-workload-1           3/3     Running   0          7d4h
    ```

1. Create a `ServiceMonitor` instance for QotD.

    ```
    oc apply -f service-monitor.yaml
    ```

1. Deploy the the alerting rules.

    ```
    oc apply -f rules_author.yml -f rules_image.yml -f rules_rating.yml -f rules_web.yml
    ```

1. Once all steps above are completed, wait about a minute or so and then validate that Prometheus metrics from QotD are scraped as expected by going to your OpenShift console and navigating to the `Monitoring` -> `Metrics` view. On the `Metrics` panel, execute the `nodejs_heap_size_used_bytes{container="qotd-image"}` query to gather the `nodejs_heap_size_used_bytes` metric from the container named `qotd-image`:

    ![ocp monitoring 1](./ocp-monitoring-1.png "OCP monitoring 1")

