# Anomaly: Rating, author, image service cascade failure

This use case simulates a failure in the ratings service.  Minutes later the failures cascade to web, author and image services. This use case executes the following steps:

1. Several new types of log entries are introduced in the Author and Image services. The templates for these are shown below.

    * Added in Rating service about every two seconds:
        ```
        WARNING - memory checksum doesn't match {{WORD}}
        ```

    * Added in Rating service about every 1.5 seconds:
        ```
        DANGER Will Robinson. Unexpected request for {{URL}} from source: {{IP}}
        ```

1. Increases memory and CPU usage in Rating service.

1. Increases latency (2 secs) in Rating service.

1. After about 10 seconds, no more anomalous log entries are introduced in the Rating service, while the introduced latency is kept.

1. After about 1 min, increases latency (2 secs) in Author service.

1. After about 30 secs, increases latency (2 secs) in Image service.

## Source

```json
{
  "id": "ratings_cascade_failure",
  "name": "Rating, author, image service cascade failure",
  "description": "This use case simulates a failure in the ratings service.  Minutes later the failures cascade to web, author and image services.  ",
  "steps": [
    {
      "name": "Start new repeating log warning about memory checksum every 2 seconds",
      "type": "add_logger",
      "service": "rating",
      "log": {
        "id": "log1",
        "name": "memory checksum",
        "template": "WARNING - memory checksum doesn't match {{WORD}} ",
        "fields": {
          "WORD": {
            "type": "word"
          }
        },
        "repeat": {
          "mean": 4000,
          "stdev": 1000,
          "min": 2000,
          "max": 8000
        }
      }
    },
    {
      "name": "Start log warning about requests for resource fro IP address (author service).  Repeats every 1.5 seconds.",
      "type": "add_logger",
      "service": "rating",
      "log": {
        "id": "resreq1",
        "name": "dangerwill",
        "template": "DANGER Will Robinson. Unexpected request for {{URL}} from source: {{IP}} ",
        "fields": {
          "URL": {
            "type": "url"
          },
          "IP": {
            "type": "ip"
          }
        },
        "repeat": {
          "mean": 3500,
          "stdev": 100,
          "min": 2000,
          "max": 7000
        }
      }
    },
    {
      "name": "Increase memory usage",
      "type": "metric",
      "service": "rating",
      "metric": "mem",
      "value": {
        "hogs": 8
      }
    },
    {
      "name": "Increase cpu usage",
      "type": "metric",
      "service": "rating",
      "metric": "cpu",
      "value": {
        "hogs": 8
      }
    },
    {
      "name": "Increase service delay (2s)",
      "type": "metric",
      "service": "rating",
      "metric": "api",
      "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
      }
    },
    {
      "name": "Pause 10 seconds",
      "type": "delay",
      "duration": "10000"
    },
    {
      "name": "Remove first logger",
      "type": "remove_logger",
      "service": "rating",
      "logid": "log1"
    },
    {
      "name": "Remove second logger",
      "type": "remove_logger",
      "service": "rating",
      "logid": "resreq1"
    },
    {
      "name": "Pause a minute",
      "type": "delay",
      "duration": "60000"
    },
    {
      "name": "Increase service delay in Author service (2s)",
      "type": "metric",
      "service": "author",
      "metric": "api",
      "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
      }
    },
    {
      "name": "Pause a half a minute",
      "type": "delay",
      "duration": "30000"
    },
    {
      "name": "Increase service delay in Image service (2s)",
      "type": "metric",
      "service": "image",
      "metric": "api",
      "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
      }
    }
  ]
}
```